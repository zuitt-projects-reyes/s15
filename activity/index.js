/*
	Activity:


	Create two functions:

		-First function: oddEvenChecker
			-This function will check if the number input or passed as an argument is an odd or even number.
				-Check if the argument being passed is a number or not.
					-if it is a number, check if the number is odd or even.
					-even numbers are divisible by 2.
					-log a message in the console: "The number is even." if the number passed as argument is even.
					-log a message in the console: "The number is odd." if the number passed as argument is odd.
					-if the number passed is not a number type: 
						show an alert:
						"Invalid Input."

		-Second Function: budgetChecker()
			-This function will check if the number input or passed as an argument is over or is less than the recommended budget.
				-Check if the argument being is a number or not.
					-if it is a number check if the number given is greater than 40000
						-log a message in the console: ("You are over the budget.")
					-if the number is not over 40000:
						-log a message in the console: ("You have resources left.")
					-if the argument passed is not a number:
						-show an alert: ("Invalid Input.")


*/

// For #1
function oddEvenChecker(num) {
	if (typeof(num) !== 'number') {
		alert('Invalid Input.');
	} else if (num % 2 === 0) {
		console.log('The number is even.')
	} else {
		console.log('The number is odd.')
	}
}

oddEvenChecker(12);
oddEvenChecker(15);

// For #2
function budgetChecker(budget) {
	if (typeof(budget) !== 'number') {
		alert('Invalid Input.');
	} else if (budget > 40000) {
		console.log('You are over the budget.');
	} else {
		console.log('You have resources left');
	}
}

budgetChecker(500000);
budgetChecker(20);
